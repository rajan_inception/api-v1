package main

import (
	"log"
	"net/http"

	"github.com/emicklei/go-restful"
	"github.com/emicklei/go-restful/swagger"
	"api/controllers"
	"os"

	_ "github.com/lib/pq"
	"api/libs/database"
	"api/libs/system"
)

func main() {
	os.Setenv(system.Env, system.EnvDevelopment)
	// to see what happens in the package, uncomment the following
	restful.TraceLogger(log.New(os.Stdout, "[restful] ", log.LstdFlags | log.Lshortfile))

	wsContainer := restful.NewContainer()
	controllers.UserResource{}.Register(wsContainer)
	controllers.CompanyResource{}.Register(wsContainer)
	controllers.AuthenticationResource{}.Register(wsContainer)
	controllers.SiteResource{}.Register(wsContainer)

	// Add container filter to enable CORS
	cors := restful.CrossOriginResourceSharing{
		ExposeHeaders:  []string{"X-My-Header"},
		AllowedHeaders: []string{"Content-Type", "Accept"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "ORIGIN"},
		CookiesAllowed: false,
		Container:      wsContainer}
	wsContainer.Filter(cors.Filter)

	// Add container filter to respond to OPTIONS
	wsContainer.Filter(wsContainer.OPTIONSFilter)

	database.DB, _ = database.GetDB()

	// Optionally, you can install the Swagger Service which provides a nice Web UI on your REST API
	// You need to download the Swagger HTML5 assets and change the FilePath location in the config below.
	// Open http://localhost:8080/apidocs and enter http://localhost:8080/apidocs.json in the api input field.
	config := swagger.Config{
		WebServices:    wsContainer.RegisteredWebServices(), // you control what services are visible
		WebServicesUrl: "http://localhost:8008",
		ApiPath:        "/apidocs.json",

		// Optionally, specifiy where the UI is located
		SwaggerPath:     "/apidocs/",
		SwaggerFilePath: "node_modules/swagger-ui/dist"}
	swagger.RegisterSwaggerService(config, wsContainer)

	log.Printf("start listening on localhost:8008")
	server := &http.Server{Addr: ":8008", Handler: wsContainer}
	log.Fatal(server.ListenAndServe())
}