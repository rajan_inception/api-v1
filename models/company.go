package models

import (
	"api/libs/models"
	"api/libs/permission"
	"errors"
	"time"
)

// Company ...
type Company struct {
	ID          int64      `gorm:"primary_key"`
	Name        string     `json:"name" validate:"required,gte=2,lte=30"`
	CompanyCode string     `json:"company_code" validate:"lte=5"`
	Abn         string     `json:"abn" validate:"omitempty,len=11"`
	Description string     `json:"description"`
	VerifiedOn  *time.Time `json:"verified_on"`
	OwnerID     int64      `json:"owner_id" validate:"required"`
	AddressID   int64      `json:"address_id" validate:"required"`
	models.Model

	//Subscription *Subscription  `json:"rel(fk);null"`
	Address *Address `json:"address"`
	Owner   *Person  `json:"owner"`
	Sites   *[]Site  `json:"sites"`
}

// TableName ...
func (c Company) TableName() string {
	return "companies"
}

// GetPermissions ...
func (c Company) GetPermissions() map[string]permission.Permission {
	permissions := map[string]permission.Permission{
		permission.ActionCreate: {
			ID:    c.TableName() + ":" + permission.ActionCreate,
			Roles: []permission.Role{permission.SuperAdmin, permission.Admin, permission.Owner},
		},
		permission.ActionUpdate: {
			ID:    c.TableName() + ":" + permission.ActionUpdate,
			Roles: []permission.Role{permission.SuperAdmin, permission.Admin, permission.Owner, permission.Manager},
		},
		permission.ActionDelete: {
			ID:    c.TableName() + ":" + permission.ActionDelete,
			Roles: []permission.Role{permission.SuperAdmin, permission.Owner},
		},
	}

	for k, v := range c.Model.GetPermissions() {
		_, ok := permissions[k]
		if !ok {
			permissions[k] = v
		}
	}

	return permissions
}

// SetCompanyID ...
func (c Company) SetCompanyID(companyID int64) models.ModelInterface {
	return &c
}

// SetSiteID ...
func (c Company) SetSiteID(siteID int64) models.ModelInterface {
	return &c
}

// GetID ...
func (c Company) GetID() int64 {
	return c.ID
}

// BeforeCreate ...
func (c *Company) BeforeCreate() error {
	return c.Model.BeforeCreate(c)
}

// BeforeUpdate ...
func (c *Company) BeforeUpdate() error {
	return c.Model.BeforeUpdate(c)
}

// PrepareModel ...
func (c Company) PrepareModel(insert bool) (models.ModelInterface, error) {
	if c.Address != nil {
		address := new(Address)
		err := c.Prepare(c.Address, address, insert)
		if err != nil {
			return &c, err
		}
		if c.AddressID != 0 && c.Address.ID != c.AddressID {
			return &c, errors.New("Invalid Address Id")
		}
		c.AddressID = c.Address.ID
	}

	if insert {
		if c.CurrentPersonID == 0 {
			return &c, errors.New("Current user has not been authenticated")
		}
		c.OwnerID = c.CurrentPersonID
	} else if c.Owner != nil {
		person := new(Person)
		err := c.Prepare(c.Owner, person, insert)
		if err != nil {
			return &c, err
		}
		if c.OwnerID != 0 && c.Owner.ID != c.OwnerID {
			return &c, errors.New("Invalid Owner Id")
		}
		c.OwnerID = c.Owner.ID
	}
	return &c, nil
}

// SetCurrentPersonID ...
func (c Company) SetCurrentPersonID(id int64) models.ModelInterface {
	c.CurrentPersonID = id
	return &c
}
