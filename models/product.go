package models

import (
	"api/libs/models"
	"errors"
)

// Product ...
type Product struct {
	ID          int64   `gorm:"primary_key"`
	SiteID      int64   `json:"site_id"`
	Name        string  `json:"name"`
	Code        string  `json:"code"`
	Description string  `json:"description"`
	Barcode     string  `json:"barcode"`
	CostExTax   float32 `json:"cost_ex_tax"`
	CostTax     float32 `json:"cost_tax"`
	IsForSale   bool    `json:"is_for_sale"`
	IsModifier  bool    `json:"is_modifier"`
	models.Model

	Site        *Site `json:"site"`
	Variants    []*Product `gorm:"many2many:product_variants;"`
	Modifiers   []*Product `gorm:"many2many:product_modifiers;"`
}

// TableName ...
func (p *Product) TableName() string {
	return "products"
}

// GetID ...
func (p Product) GetID() int64 {
	return p.ID
}

// IsSiteContext ...
func (p Product) IsSiteContext() bool {
	return true
}

// PrepareModel ...
func (p Product) PrepareModel(insert bool) (models.ModelInterface, error) {
	if p.Site != nil {
		site := new(Site)
		err := p.Prepare(p.Site, site, insert)
		if err != nil {
			return &p, err
		}
		if p.SiteID != 0 && p.Site.ID != p.SiteID {
			return &p, errors.New("Invalid Company Id")
		}
		p.SiteID = p.Site.ID
	}

	if len(p.Modifiers) > 0 {
		for _, mod := range p.Modifiers {
			mod.SiteID = p.SiteID
			mod.IsModifier = true
			// set Id if code is matched
		}
	}
	return &p, nil
}

// BeforeCreate ...
func (p *Product) BeforeCreate() error {
	return p.Model.BeforeCreate(p)
}

// BeforeUpdate ...
func (p *Product) BeforeUpdate() error {
	return p.Model.BeforeUpdate(p)
}

// SetSiteID ...
func (p Product) SetSiteID(siteID int64) models.ModelInterface {
	p.SiteID = siteID
	return &p
}

// SetCurrentPersonID ...
func (p Product) SetCurrentPersonID(id int64) models.ModelInterface {
	p.CurrentPersonID = id
	return &p
}
