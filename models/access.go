package models

import (
	"time"
)

// Access structure to store user access data
type Access struct {
	AccessToken   string     `gorm:"primary_key" json:"access_token"`
	RefreshToken  string     `json:"refresh_token"`
	ClientID      string     `json:"client_id"`
	AuthorizeCode string     `json:"authorize_code"`
	ExpiresIn     int32      `json:"expires_in"`
	Scope         string     `json:"scope"`
	RedirectURI   string     `json:"redirect_uri"`
	Previous      string     `json:"previous"`
	Extra         string     `json:"extra"`
	UserID        int64      `json:"user_id"`
	CreatedAt     time.Time  `json:"created_at"`
	UpdatedAt     time.Time  `json:"updated_at"`
	DeletedAt     *time.Time `sql:"index"`
}

// TableName ...
func (Access) TableName() string {
	return "oauth_accesses"
}
