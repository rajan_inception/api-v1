package models

import (
	"api/libs/database"
	"api/libs/models"
	"time"

	"github.com/iris-contrib/errors"
)

// Person ...
type Person struct {
	ID          int64      `gorm:"primary_key"`
	Contact     *Contact   `json:"contact" validate:"required"`
	Address     *Address   `json:"address"`
	User        *User      `json:"user"`
	ActiveSince *time.Time `json:"active_since"`
	models.Model
}

// TableName ...
func (p Person) TableName() string {
	return "people"
}

// GetID ...
func (p Person) GetID() int64 {
	return p.ID
}

// SetCompanyID ...
func (p Person) SetCompanyID(int64) models.ModelInterface {
	return &p
}

// SetSiteID ...
func (p Person) SetSiteID(int64) models.ModelInterface {
	return &p
}

// PrepareModel ...
func (p Person) PrepareModel(insert bool) (models.ModelInterface, error) {
	return &p, nil
}

// BeforeCreate ...
func (p *Person) BeforeCreate() error {
	return p.Model.BeforeCreate(p)
}

// BeforeUpdate ...
func (p *Person) BeforeUpdate() error {
	return p.Model.BeforeUpdate(p)
}

// GetUser ...
func (p *Person) GetUser() (user User, err error) {
	userExists := database.DB.Where("person_id = ?", p.ID).First(&user).RecordNotFound()
	if !userExists {
		err = errors.New("User does not exists")
		return
	}
	return user, nil
}
