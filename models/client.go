package models

import (
	"api/libs/models"
)

// Client ...
type Client struct {
	ID          int64  `gorm:"primary_key"`
	Secret      string `json:"secret"`
	Extra       string `json:"extra"`
	RedirectURI string `json:"redirect_uri"`
	models.Model
}

// TableName ...
func (Client) TableName() string {
	return "oauth_clients"
}

// BeforeCreate ...
func (c *Client) BeforeCreate() error {
	return c.Model.BeforeCreate(c)
}

// BeforeUpdate ...
func (c *Client) BeforeUpdate() error {
	return c.Model.BeforeUpdate(c)
}
