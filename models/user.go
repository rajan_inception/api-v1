package models

import (
	"api/libs/database"
	"api/libs/models"
	"api/libs/permission"
	"api/libs/util"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

// User ...
type User struct {
	ID          int64  `gorm:"primary_key"`
	Username    string `json:"username" validate:"required"`
	Password    string `json:"-" validate:"required"`
	Seed        string `json:"-"`
	Roles       string `json:"_"`
	Permissions string `json:"_"`
	PersonID    int64  `json:"person_id"`
	models.Model

	Person *Person
}

// TableName ...
func (User) TableName() string {
	return "users"
}

// BeforeCreate ...
func (u *User) BeforeCreate() error {
	u.Seed = util.RandomString(16)
	u.Password = u.Password + u.Seed
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	u.Password = string(hashedPassword)
	return u.Model.BeforeCreate(u)
}

// BeforeUpdate ...
func (u *User) BeforeUpdate() error {
	return u.Model.BeforeUpdate(u)
}

// GetRoles ...
func (u User) GetRoles(level string, referenceID int64) (roles []permission.Role) {
	roleString, _ := permission.GetUserAccesses(u.Roles)
	for i := range roleString {
		if (roleString[i].Level == level && roleString[i].ReferenceID == referenceID) ||
			roleString[i].PermissionName == permission.SuperAdmin.ID ||
			roleString[i].PermissionName == permission.Admin.ID {
			role, found := permission.GetRoleByID(roleString[i].PermissionName)
			if found > 0 {
				roles = append(roles, role)
			}
		}
	}
	return
}

// HasPermission ...
func (u User) HasPermission(per permission.Permission, CompanyID int64) bool {
	permissionString := strings.Split(u.Permissions, ",")

	for i := range permissionString {
		permissionID := permissionString[i]
		if permissionID == per.ID {
			return true
		}
	}

	userRoles := u.GetRoles(permission.CompanyLevel, CompanyID)
	for j := range userRoles {
		if per.CheckRoleHavePermission(userRoles[j]) {
			return true
		}
	}

	return false
}

// AddRole ...
func (u *User) AddRole(name string, level string, referenceID int64) error {
	upss, err := permission.AddUserAccess(u.Roles, name, level, referenceID)
	if err != nil {
		return err
	}
	u.Roles, err = permission.EncodeUserAccess(upss)
	if err != nil {
		return err
	}
	database.DB.Save(u)
	return nil
}

// GetPerson ...
func (u *User) GetPerson() (person Person) {
	database.DB.First(&person, u.PersonID)
	return
}
