package models

import (
	"time"
)

// Authorize ...
type Authorize struct {
	Code        string         `gorm:"primary_key" json:"code"`
	ClientID    string         `json:"client_id"`
	ExpiresIn   string         `json:"expires_in"`
	Scope       string         `json:"scope"`
	RedirectURI string         `json:"redirect_uri"`
	State       string         `json:"state"`
	Extra       string         `json:"extra"`
	CreatedAt   time.Time      `json:"created_at);auto_now_add"`
	UpdatedAt   time.Time      `json:"updated_at);auto_now"`
	DeletedAt   *time.Time     `sql:"index"`
}

// TableName ...
func (Authorize) TableName() string {
	return "oauth_authorizations"
}
