package models

import "api/libs/models"

// Contact ...
type Contact struct {
	ID         int64  `gorm:"primary_key"`
	FirstName  string `json:"first_name" validate:"required"`
	MiddleName string `json:"middle_name"`
	LastName   string `json:"last_name"`
	Website    string `json:"website"`
	Note       string `json:"note"`
	models.Model
}

// TableName ...
func (t Contact) TableName() string {
	return "contacts"
}
