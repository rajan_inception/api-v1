package models

import (
	"api/libs/models"
	"api/libs/permission"
	"errors"
)

// Site ...
type Site struct {
	ID          int64    `gorm:"primary_key"`
	Name        string   `json:"name" validate:"required,gte=2,lte=30"`
	Code        string   `json:"code" validate:"lte=5"`
	Description string   `json:"description"`
	Timezone    string   `json:"timezone"`
	CompanyID   int64    `json:"company_id" validate:"required"`
	Company     *Company `json:"company"`
	models.Model
}

// TableName ...
func (s Site) TableName() string {
	return "sites"
}

// GetPermissions ...
func (s Site) GetPermissions() map[string]permission.Permission {
	return map[string]permission.Permission{
		permission.ActionCreate: {
			ID:    s.TableName() + ":" + permission.ActionCreate,
			Roles: []permission.Role{permission.SuperAdmin, permission.Admin, permission.Owner, permission.Manager},
		},
		permission.ActionUpdate: {
			ID:    s.TableName() + ":" + permission.ActionUpdate,
			Roles: []permission.Role{permission.SuperAdmin, permission.Admin, permission.Owner, permission.Manager},
		},
		permission.ActionDelete: {
			ID:    s.TableName() + ":" + permission.ActionDelete,
			Roles: []permission.Role{permission.SuperAdmin, permission.Owner, permission.Owner},
		},
	}
}

// IsSiteContext ...
func (s Site) IsSiteContext() bool {
	return false
}

// IsCompanyContext ...
func (s Site) IsCompanyContext() bool {
	return true
}

// SetCompanyID ...
func (s Site) SetCompanyID(companyID int64) models.ModelInterface {
	s.CompanyID = companyID
	return &s
}

// SetSiteID ...
func (s Site) SetSiteID(siteID int64) models.ModelInterface {
	return &s
}

// GetID ...
func (s Site) GetID() int64 {
	return s.ID
}

// PrepareModel ...
func (s Site) PrepareModel(insert bool) (models.ModelInterface, error) {
	if s.Company != nil {
		company := new(Company)
		err := s.Prepare(s.Company, company, insert)
		if err != nil {
			return &s, err
		}
		if s.CompanyID != 0 && s.Company.ID != s.CompanyID {
			return &s, errors.New("Invalid Company Id")
		}
		s.CompanyID = s.Company.ID
	}
	return &s, nil
}

// BeforeCreate ...
func (s *Site) BeforeCreate() error {
	return s.Model.BeforeCreate(s)
}

// BeforeUpdate ...
func (s *Site) BeforeUpdate() error {
	return s.Model.BeforeUpdate(s)
}

// SetCurrentPersonID ...
func (s Site) SetCurrentPersonID(id int64) models.ModelInterface {
	s.CurrentPersonID = id
	return &s
}
