package models

import (
	"api/libs/database"
	"errors"
	"fmt"
	"reflect"
	"strconv"

	"github.com/RangelReale/osin"
)

// AuthStorage ...
type AuthStorage struct {
	clients   map[string]osin.Client
	authorize map[string]*osin.AuthorizeData
	access    map[string]*osin.AccessData
	refresh   map[string]string
}

// NewAuthStorage ...
func NewAuthStorage() *AuthStorage {
	r := &AuthStorage{
		clients:   make(map[string]osin.Client),
		authorize: make(map[string]*osin.AuthorizeData),
		access:    make(map[string]*osin.AccessData),
		refresh:   make(map[string]string),
	}

	return r
}

// Refactored code that uses database

// SaveAccess ...
func (s *AuthStorage) SaveAccess(data *osin.AccessData) error {
	fmt.Printf("SaveAccess: %s\n", data.AccessToken)
	v := reflect.ValueOf(data.UserData)
	user := v.Interface().(User)
	access := Access{
		AccessToken:  data.AccessToken,
		RefreshToken: data.RefreshToken,
		ClientID:     data.Client.GetId(),
		ExpiresIn:    data.ExpiresIn,
		Scope:        data.Scope,
		RedirectURI:  data.RedirectUri,
		UserID:       user.ID,
	}
	if data.AuthorizeData != nil {
		access.AuthorizeCode = data.AuthorizeData.Code
	}
	database.DB.Create(&access)
	return nil
}

// LoadAccess ...
func (s *AuthStorage) LoadAccess(token string) (oa *osin.AccessData, err error) {
	fmt.Printf("LoadAccess: %s\n", token)
	var access Access
	database.DB.Where("access_token = ?", token).First(&access)
	if database.DB.RecordNotFound() {
		err = errors.New("Could not load access data")
		return
	}

	var client Client
	var user User

	clientID, _ := strconv.ParseInt(access.ClientID, 10, 64)
	database.DB.First(&client, clientID)
	database.DB.First(&user, access.UserID)

	osinClient := &osin.DefaultClient{
		Id:          strconv.FormatInt(client.ID, 10),
		Secret:      client.Secret,
		RedirectUri: client.RedirectURI,
	}

	oa = &osin.AccessData{
		Client:      osinClient,
		AccessToken: access.AccessToken,
		ExpiresIn:   access.ExpiresIn,
		Scope:       access.Scope,
		RedirectUri: access.RedirectURI,
		CreatedAt:   access.CreatedAt,
		UserData:    user.ID,
	}

	return oa, nil
}

// GetClient ..
func (s *AuthStorage) GetClient(id string) (oc osin.Client, err error) {
	clientID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil, err
	}
	var client Client
	database.DB.First(&client, clientID)
	if database.DB.RecordNotFound() {
		return
	}
	oc = &osin.DefaultClient{
		Id:          strconv.FormatInt(client.ID, 10),
		Secret:      client.Secret,
		RedirectUri: client.RedirectURI,
	}
	return oc, nil
}

// Legacy Code

// Clone ...
func (s *AuthStorage) Clone() osin.Storage {
	return s
}

// Close ...
func (s *AuthStorage) Close() {
}

// SaveAuthorize ...
func (s *AuthStorage) SaveAuthorize(data *osin.AuthorizeData) error {
	fmt.Printf("SaveAuthorize: %s\n", data.Code)
	s.authorize[data.Code] = data
	return nil
}

// LoadAuthorize ...
func (s *AuthStorage) LoadAuthorize(code string) (*osin.AuthorizeData, error) {
	fmt.Printf("LoadAuthorize: %s\n", code)
	if d, ok := s.authorize[code]; ok {
		return d, nil
	}
	return nil, errors.New("Authorize not found")
}

// RemoveAuthorize ...
func (s *AuthStorage) RemoveAuthorize(code string) error {
	fmt.Printf("RemoveAuthorize: %s\n", code)
	delete(s.authorize, code)
	return nil
}

// RemoveAccess ...
func (s *AuthStorage) RemoveAccess(code string) error {
	fmt.Printf("RemoveAccess: %s\n", code)
	delete(s.access, code)
	return nil
}

// LoadRefresh ...
func (s *AuthStorage) LoadRefresh(code string) (*osin.AccessData, error) {
	fmt.Printf("LoadRefresh: %s\n", code)
	if d, ok := s.refresh[code]; ok {
		return s.LoadAccess(d)
	}
	return nil, errors.New("Refresh not found")
}

// RemoveRefresh ...
func (s *AuthStorage) RemoveRefresh(code string) error {
	fmt.Printf("RemoveRefresh: %s\n", code)
	delete(s.refresh, code)
	return nil
}
