package models

import (
	"api/libs/models"
)

// Address ...
type Address struct {
	ID            int64   `gorm:"primary_key"`
	HouseNumber   string  `json:"house_number"`
	UnitNumber    string  `json:"unit_number"`
	StreetAddress string  `json:"street_address"`
	City          string  `json:"city"`
	State         string  `json:"state"`
	Region        string  `json:"region"`
	Country       string  `json:"country"`
	PostCode      string  `json:"post_code"`
	Latitude      float32 `json:"latitude"`
	Longitude     float32 `json:"longitude"`
	models.Model
}

// TableName ...
func (a *Address) TableName() string {
	return "addresses"
}

// GetID ...
func (a Address) GetID() int64 {
	return a.ID
}

// IsSiteContext ...
func (a Address) IsSiteContext() bool {
	return false
}

// PrepareModel ...
func (a Address) PrepareModel(insert bool) (models.ModelInterface, error) {
	return &a, nil
}

// BeforeCreate ...
func (a *Address) BeforeCreate() error {
	return a.Model.BeforeCreate(a)
}

// BeforeUpdate ...
func (a *Address) BeforeUpdate() error {
	return a.Model.BeforeUpdate(a)
}
