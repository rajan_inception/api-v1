package http

import "github.com/emicklei/go-restful"

// RestResponse ...
type RestResponse struct {
	restful.Response
}

// RestError ...
type RestError struct {
	ErrorCode int
	Message   string
}
