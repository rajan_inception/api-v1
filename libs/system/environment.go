package system

// Env Environment of APP
const Env = "ENV"

// EnvProduction APP in production
const EnvProduction = "production"

// EnvDevelopment APP in development
const EnvDevelopment = "development"

// EnvTest APP in test
const EnvTest = "tests"

// EnvDevelopment APP in development
const EnvDebug = "debug"
