package database

import (
	"api/libs/system"
	"os"

	"github.com/jinzhu/gorm"

	// Needed Driver for Cockroachdb
	_ "github.com/lib/pq"
)

// DB is the connection handle for the database
var DB *gorm.DB

// GetDB Gets database connection
func GetDB() (*gorm.DB, error) {
	environment := os.Getenv(system.Env)
	connectionString := "postgresql://root@localhost:26257/irestaurant?sslmode=disable"
	if environment == system.EnvTest {
		connectionString = "postgresql://root@localhost:26257/irestaurant_test?sslmode=disable"
	}
	db, err := gorm.Open("postgres", connectionString)

	if err != nil {
		panic("failed to connect database" + err.Error())
	}

	if environment == system.EnvDebug {
		db.LogMode(true)
	}
	return db, err
}

// CloseDB ...
func CloseDB(db *gorm.DB) {
	db.Close()
}
