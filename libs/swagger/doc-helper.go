package swagger

import (
	"api/libs/models"

	"github.com/emicklei/go-restful"
	"github.com/jinzhu/inflection"
)

// DocHelper Documentation helper structure
type DocHelper struct {
	Model        models.ModelInterface
	ResourceName string
}

// ControllerDefinition Definition for controller
func (d *DocHelper) ControllerDefinition(ws *restful.WebService) {
	ws.Path("/"+d.ResourceName).
		Doc("Manage "+inflection.Singular(d.ResourceName)).
		Consumes(restful.MIME_XML, restful.MIME_JSON).
		Produces(restful.MIME_JSON, restful.MIME_XML) // you can specify this per route as well
}

// GetAll ...
func (d *DocHelper) GetAll(rb *restful.RouteBuilder, ws *restful.WebService, addToRoute bool) {
	singular := inflection.Singular(d.ResourceName)
	rb.Doc("get all " + d.ResourceName).
		Operation("get" + singular)
	d.AddParams(rb, ws)
	rb.Writes(d.Model) // on the response

	if addToRoute {
		ws.Route(rb)
	}
}

// Get ...
func (d *DocHelper) Get(rb *restful.RouteBuilder, ws *restful.WebService, addToRoute bool) {
	singular := inflection.Singular(d.ResourceName)
	rb.Doc("get one " + d.ResourceName).
		Operation("get" + singular)
	d.AddParams(rb, ws)
	rb.Param(ws.PathParameter(singular+"-id", "identifier of "+singular).DataType("string")).
		Writes(d.Model) // on the response

	if addToRoute {
		ws.Route(rb)
	}
}

// Update ...
func (d *DocHelper) Update(rb *restful.RouteBuilder, ws *restful.WebService, addToRoute bool) {
	singular := inflection.Singular(d.ResourceName)
	rb.Doc("update " + singular).
		Operation("update" + singular)
	d.AddParams(rb, ws)
	rb.Param(ws.PathParameter(singular+"-id", "identifier of "+singular).DataType("string")).
		Returns(409, "duplicate "+singular+"-id", nil).
		Reads(d.Model) // from the request

	if addToRoute {
		ws.Route(rb)
	}
}

// Create ...
func (d *DocHelper) Create(rb *restful.RouteBuilder, ws *restful.WebService, addToRoute bool) {
	singular := inflection.Singular(d.ResourceName)
	rb.Doc("create " + singular).
		Operation("create" + singular)
	d.AddParams(rb, ws)
	rb.Reads(d.Model) // from the request

	if addToRoute {
		ws.Route(rb)
	}
}

// Delete ...
func (d *DocHelper) Delete(rb *restful.RouteBuilder, ws *restful.WebService, addToRoute bool) {
	singular := inflection.Singular(d.ResourceName)
	rb.Doc("delete " + singular).
		Operation("remove" + singular)
	d.AddParams(rb, ws)
	rb.Param(ws.PathParameter(singular+"-id", "identifier of "+singular).DataType("string"))

	if addToRoute {
		ws.Route(rb)
	}
}

// AddParams Add extra params to doc
func (d *DocHelper) AddParams(rb *restful.RouteBuilder, ws *restful.WebService) {
	if d.Model.IsCompanyContext() {
		rb.Param(ws.PathParameter("company-id", "identifier of company").DataType("string"))
	} else if d.Model.IsSiteContext() {
		rb.Param(ws.PathParameter("site-id", "identifier of site").DataType("string"))
	}
}
