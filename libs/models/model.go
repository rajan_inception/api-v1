package models

import (
	"api/libs/database"
	"api/libs/permission"
	"errors"
	"time"

	"github.com/jinzhu/inflection"
	"gopkg.in/go-playground/validator.v9"
)

// ModelInterface Generic interface for models
type ModelInterface interface {
	TableName() string
	IsSiteContext() bool
	IsCompanyContext() bool
	SetCompanyID(int64) ModelInterface
	SetSiteID(int64) ModelInterface
	GetID() int64
	PrepareModel(bool) (ModelInterface, error)

	GetValidationError() map[string]string
	SetCurrentPersonID(int64) ModelInterface
	IsValid() bool
}

// Model base structure for Models
type Model struct {
	CreatedAt       time.Time
	UpdatedAt       time.Time
	DeletedAt       *time.Time        `sql:"index"`
	ValidationError map[string]string `gorm:"-" json:"-"`
	CurrentPersonID int64             `gorm:"-" json:"-"`
}

// TableName Database table name
func (m Model) TableName() string {
	return ""
}

// GetPermissions Default permission slices
func (m Model) GetPermissions() map[string]permission.Permission {
	return map[string]permission.Permission{
		permission.ActionCreate: {
			ID:    "ROOT:" + permission.ActionCreate,
			Roles: []permission.Role{permission.SuperAdmin},
		},
		permission.ActionRead: {
			ID:    "ROOT:" + permission.ActionRead,
			Roles: []permission.Role{permission.SuperAdmin},
		},
		permission.ActionUpdate: {
			ID:    "ROOT:" + permission.ActionUpdate,
			Roles: []permission.Role{permission.SuperAdmin},
		},
		permission.ActionDelete: {
			ID:    "ROOT:" + permission.ActionDelete,
			Roles: []permission.Role{permission.SuperAdmin},
		},
	}
}

// Prepare This method will create/update other dependencies models based on parameter provided
func (m *Model) Prepare(originalAddress ModelInterface, mi ModelInterface, insert bool) error {

	id := originalAddress.GetID()
	singular := inflection.Singular(mi.TableName())

	// Fixme: make passing mi work for both update and insert
	// Currently create does not work if we use mi and then copy the
	// address to originalAddress
	// This is update specific call
	if insert == false {
		if id != 0 {
			recordNotFound := database.DB.First(mi, id).RecordNotFound()
			if recordNotFound {
				return errors.New(singular + " does not exist for given ID.")
			}
			originalAddress = mi
		}
	}

	// This is insert specific call
	// This requires originalAddress to be passed
	if insert == true {
		if id != 0 {
			recordNotFound := database.DB.First(originalAddress, id).RecordNotFound()
			if recordNotFound {
				return errors.New(singular + " does not exist for given ID.")
			}
		} else if id == 0 && insert == true {
			dbErrors := database.DB.Create(originalAddress).GetErrors()
			if len(dbErrors) > 0 {
				return dbErrors[0]
			} else if database.DB.NewRecord(originalAddress) {
				return errors.New("Failed creating " + singular)
			}
		}
	}

	return nil
}

// BeforeCreate validates provided model.
// This has to be called from child class otherwise ends up with invalid method error
func (m *Model) BeforeCreate(c ModelInterface) (err error) {
	c, err = c.PrepareModel(true)
	if err != nil {
		return err
	}
	errs := m.Validate(c)
	if errs != nil {
		m.SetValidationError(errs)
		return errors.New("Validation failed")
	}
	return
}

// BeforeUpdate validates provided model.
// This has to be called from child class otherwise ends up with invalid method error
func (m *Model) BeforeUpdate(c ModelInterface) (err error) {
	errs := m.Validate(c)
	if errs != nil {
		m.SetValidationError(errs)
		return errors.New("Validation failed")
	}
	c, err = c.PrepareModel(false)
	if err != nil {
		return err
	}
	return
}

// GetValidationError Returns validation errors
func (m Model) GetValidationError() map[string]string {
	return m.ValidationError
}

// SetValidationError Sets validation error
func (m *Model) SetValidationError(errors map[string]string) {
	m.ValidationError = errors
}

// IsValid return true if valid
func (m Model) IsValid() bool {
	return len(m.GetValidationError()) == 0
}

// GetID return model ID
func (m Model) GetID() int64 {
	return 0
}

// PrepareModel ...
func (m Model) PrepareModel(insert bool) (ModelInterface, error) {
	return m, nil
}

// SetCompanyID ...
func (m Model) SetCompanyID(companyID int64) ModelInterface {
	return &m
}

// SetSiteID ...
func (m Model) SetSiteID(siteID int64) ModelInterface {
	return &m
}

// IsSiteContext ...
func (m Model) IsSiteContext() bool {
	return false
}

// IsCompanyContext ...
func (m Model) IsCompanyContext() bool {
	return false
}

// Validate Validates given Model
func (m *Model) Validate(model ModelInterface) (errs map[string]string) {
	validate := validator.New()
	validationErr := validate.Struct(model)
	if validationErr != nil {
		errs = make(map[string]string)
		for _, vErr := range validationErr.(validator.ValidationErrors) {
			errs[vErr.StructNamespace()] = vErr.StructField() + " " + vErr.Tag() + " " + vErr.Param()
		}
		return errs
	}
	return
}

// SetCurrentPersonID Sets logged in person as current person
func (m Model) SetCurrentPersonID(id int64) ModelInterface {
	m.CurrentPersonID = id
	return &m
}
