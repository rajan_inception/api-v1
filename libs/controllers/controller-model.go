package controllers

import libModel "api/libs/models"
import (
	"api/models"

	"github.com/emicklei/go-restful"
)

// ControllerModel is combination of model and controller
// which is used heavily on rest-controller
type ControllerModel struct {
	Model libModel.ModelInterface

	IsSiteContext    bool
	IsCompanyContext bool
	Company          *models.Company
	Site             *models.Site
	ServiceError     restful.ServiceError
	StatusCode       int
}
