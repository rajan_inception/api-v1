package controllers

import (
	"api/libs/authentication"
	"api/libs/database"
	"api/libs/permission"
	"api/models"
	"strconv"

	"github.com/emicklei/go-restful"
	"github.com/jinzhu/inflection"
)

// RestControllerInterface Interface for RestController
type RestControllerInterface interface {
}

// RestController Wrapper controller for Rest Resources
type RestController struct {
	ControllerModel *ControllerModel
	CurrentUser     *authentication.CurrentUser
}

// Init initiates authentication and site/company context
// This method also initialises the response object
func (c *RestController) Init(request *restful.Request, response *restful.Response) {
	// Check if controller is in site or company context and save site/company in
	// controllerModel
	if c.ControllerModel.IsCompanyContext {
		c.ResolveCompanyContext(request)
	}
	if c.ControllerModel.IsSiteContext {
		c.ResolveSiteContext(request)
	}
	c.CurrentUser, c.ControllerModel.ServiceError = authentication.GetCurrentUser(request)
	if (c.ControllerModel.ServiceError == restful.ServiceError{}) {
		c.ControllerModel.Model = c.ControllerModel.Model.SetCurrentPersonID(c.CurrentUser.Person.ID)
	}
	c.InitResponse(request, response)
}

// Get Wrapper for GET Resource Action
func (c *RestController) Get(request *restful.Request, response *restful.Response) {
	c.Init(request, response)
	c.GetModel(request)
	c.CheckAndCreateError(request, response)
}

// GetAll Wrapper for GET ALL Resource Action
func (c *RestController) GetAll(request *restful.Request, response *restful.Response) {
	c.Init(request, response)
	c.CheckAndCreateError(request, response)
}

// Create Wrapper for Create Resource Action
func (c *RestController) Create(request *restful.Request, response *restful.Response) {
	c.Init(request, response)
	err := request.ReadEntity(c.ControllerModel.Model)
	if err != nil {
		c.ControllerModel.ServiceError = restful.NewError(400, err.Error())
		return
	}
	if c.ControllerModel.Model.IsCompanyContext() {
		c.ControllerModel.Model = c.ControllerModel.Model.SetCompanyID(c.ControllerModel.Company.ID)
	}
	dbErrors := database.DB.Create(c.ControllerModel.Model).GetErrors()
	if len(dbErrors) > 0 {
		c.ControllerModel.ServiceError = restful.NewError(500, dbErrors[0].Error())
	} else if database.DB.NewRecord(c.ControllerModel.Model) {
		c.ControllerModel.ServiceError = restful.NewError(500, "Failed creating "+c.ControllerModel.Model.TableName())
	}
	c.CheckAndCreateError(request, response)
}

// Update Wrapper for Update Resource Action
func (c *RestController) Update(request *restful.Request, response *restful.Response) {
	c.Init(request, response)
	recordNotFound := c.GetModel(request)
	if !recordNotFound {
		err := request.ReadEntity(c.ControllerModel.Model)
		if err != nil {
			c.ControllerModel.ServiceError = restful.NewError(400, err.Error())
		} else {
			dbErrors := database.DB.Save(c.ControllerModel.Model).GetErrors()
			if len(dbErrors) > 0 {
				c.ControllerModel.ServiceError = restful.NewError(500, dbErrors[0].Error())
			}
		}
	}
	c.CheckAndCreateError(request, response)
}

// Delete Wrapper for Delete Resource Action
func (c *RestController) Delete(request *restful.Request, response *restful.Response) {
	c.Init(request, response)
	recordNotFound := c.GetModel(request)
	if !recordNotFound {
		dbErrors := database.DB.Delete(c.ControllerModel.Model).GetErrors()
		if len(dbErrors) > 0 {
			c.ControllerModel.ServiceError = restful.NewError(500, dbErrors[0].Error())
		}
	}
	c.CheckAndCreateError(request, response)
}

// ResolveCompanyContext grabs and checks if the request is in company context
// If the request is in company context then the company value is stored in
// ControllerModel
func (c *RestController) ResolveCompanyContext(request *restful.Request) {
	companyIDString := request.PathParameter("company-id")
	companyID, err := strconv.ParseInt(companyIDString, 10, 64)

	if err != nil {
		c.ControllerModel.ServiceError = restful.NewError(400, "Invalid Company ID.")
	}

	// TODO : cache company search
	company := models.Company{}
	recordNotFound := database.DB.First(&company, companyID).RecordNotFound()
	if recordNotFound {
		c.ControllerModel.ServiceError = restful.NewError(400, "Company does not exists.")
		return
	}
	c.ControllerModel.Company = &company
}

// ResolveSiteContext grabs and check if the request is in site context
// If the request is in site context then the site value is stored in
// ControllerModel
func (c *RestController) ResolveSiteContext(request *restful.Request) {
	siteIDString := request.PathParameter("site-id")
	siteID, err := strconv.ParseInt(siteIDString, 10, 64)

	if err != nil {
		c.ControllerModel.ServiceError = restful.NewError(400, "Invalid Site ID.")
	}

	// TODO : cache site search
	site := models.Site{}
	recordNotFound := database.DB.First(&site, siteID).RecordNotFound()
	if recordNotFound {
		c.ControllerModel.ServiceError = restful.NewError(400, "Site does not exists.")
		return
	}
	c.ControllerModel.Site = &site
}

// InitResponse initialises response
func (c *RestController) InitResponse(request *restful.Request, response *restful.Response) {
	requestContentType := request.HeaderParameter("Content-Type")
	switch requestContentType {
	case restful.MIME_JSON:
		response.AddHeader("Content-Type", restful.MIME_JSON)
		break
	case restful.MIME_XML:
		response.AddHeader("Content-Type", restful.MIME_XML)
		break
	default:
		response.AddHeader("Content-Type", restful.MIME_JSON)
	}
}

// CheckAndCreateError Adds error data in response string if error is found
func (c *RestController) CheckAndCreateError(request *restful.Request, response *restful.Response) {
	if !c.ControllerModel.Model.IsValid() {
		response.WriteHeaderAndEntity(422, c.ControllerModel.Model.GetValidationError())
	} else if (c.ControllerModel.ServiceError != restful.ServiceError{}) {
		response.WriteHeaderAndEntity(c.ControllerModel.ServiceError.Code, c.ControllerModel.ServiceError)
	}
}

// GetModel Gets model for current Resource
// If Model does not exist then it will create and add error
func (c *RestController) GetModel(request *restful.Request) bool {
	singular := inflection.Singular(c.ControllerModel.Model.TableName())
	modelID := request.PathParameter(singular + "-id")
	modelIntID, err := strconv.ParseInt(modelID, 10, 64)
	if err != nil {
		c.ControllerModel.ServiceError = restful.NewError(400, singular+" does not exists.")
	}
	recordNotFound := database.DB.First(c.ControllerModel.Model, modelIntID).RecordNotFound()
	if recordNotFound {
		c.ControllerModel.ServiceError = restful.NewError(400, singular+" does not exists.")
	}
	return recordNotFound
}

// CheckUserHasCompanyPermission checks if the currently logged in
// user have permission based on provided company and permission
func (c *RestController) CheckUserHasCompanyPermission(request *restful.Request, response *restful.Response, per permission.Permission, company *models.Company) bool {
	if c.CurrentUser == nil {
		c.ControllerModel.ServiceError = restful.NewError(401, "Current user does not have permission")
		c.CheckAndCreateError(request, response)
		return false
	}
	var companyID int64
	if company != nil {
		companyID = company.ID
	}

	userHasPermission := c.CurrentUser.User.HasPermission(per, companyID)
	if userHasPermission == false {
		c.ControllerModel.ServiceError = restful.NewError(401, "Current user does not have permission")
		c.CheckAndCreateError(request, response)
		return false
	}
	return true
}
