package authentication

import (
	"api/libs/database"
	"api/models"

	"github.com/RangelReale/osin"
	"github.com/emicklei/go-restful"
)

// CurrentUser is the user currently in context
type CurrentUser struct {
	User      models.User
	Person    models.Person
	Companies []models.Company
	Sites     []models.Site
}

// GetCurrentUser gets currently logged in user
func GetCurrentUser(request *restful.Request) (currentUser *CurrentUser, err restful.ServiceError) {
	server := osin.NewServer(osin.NewServerConfig(), &models.AuthStorage{})
	resp := server.NewResponse()
	defer resp.Close()
	if ir := server.HandleInfoRequest(resp, request.Request); ir != nil {
		userID := ir.AccessData.UserData.(int64)
		// TODO: cache
		var person models.Person
		var user models.User
		var companies []models.Company
		var sites []models.Site
		database.DB.First(&user, userID)
		database.DB.First(&person, user.PersonID)
		database.DB.Where("owner_id = ?", person.ID).Find(&companies)
		sitesQuery := database.DB.Where("companies.owner_id = ? AND companies.deleted_at IS NULL", person.ID)
		sitesQuery = sitesQuery.Joins("JOIN companies on companies.id = sites.company_id")
		sitesQuery.Find(&sites)
		currentUser = &CurrentUser{
			User:      user,
			Person:    person,
			Companies: companies,
			Sites:     sites,
		}
		server.FinishInfoRequest(resp, request.Request, ir)
		return
	}
	err = restful.NewError(401, resp.Output["error_description"].(string))
	return
}
