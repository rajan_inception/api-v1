package helpers

import (
	"encoding/json"
	"encoding/xml"

	"gopkg.in/yaml.v2"
)

// InterfaceHelper ...
type InterfaceHelper struct{}

// StringTypeXML ...
const StringTypeXML = "XML"

// StringTypeJSON ...
const StringTypeJSON = "JSON"

// StringTypeYML ...
const StringTypeYML = "YML"

// StringToInterface Converts string to interface
func StringToInterface(str string, value interface{}, stringType string) (interface{}, error) {
	ih := InterfaceHelper{}
	switch stringType {
	case StringTypeJSON:
		return ih.jsonToInterface(str, value)
	case StringTypeXML:
		return ih.xmlToInterface(str, value)
	case StringTypeYML:
		return ih.ymlToInterface(str, value)
	default:
		return ih.jsonToInterface(str, value)
	}
}

func (i InterfaceHelper) xmlToInterface(xmlString string, value interface{}) (interface{}, error) {
	if err := xml.Unmarshal([]byte(xmlString), value); err != nil {
		return "", err
	}
	js, err := json.Marshal(value)
	if err != nil {
		return "", err
	}
	return js, nil
}

func (i InterfaceHelper) jsonToInterface(jsonString string, value interface{}) (interface{}, error) {
	if err := json.Unmarshal([]byte(jsonString), value); err != nil {
		return "", err
	}
	js, err := json.Marshal(value)
	if err != nil {
		return "", err
	}
	return js, nil
}

func (i InterfaceHelper) ymlToInterface(ymlString string, value interface{}) (interface{}, error) {
	if err := yaml.Unmarshal([]byte(ymlString), value); err != nil {
		return "", err
	}
	js, err := json.Marshal(value)
	if err != nil {
		return "", err
	}
	return js, nil
}
