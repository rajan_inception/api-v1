package permission

// Permission Permission structure
type Permission struct {
	ID          string
	Name        string
	Description string
	Child       *Permission
	Roles       []Role
}

// ActionCreate ...
const ActionCreate = "CREATE"

// ActionRead ...
const ActionRead = "READ"

// ActionUpdate ...
const ActionUpdate = "UPDATE"

// ActionDelete ..
const ActionDelete = "DELETE"

// CheckRoleHavePermission Checks if role has permission
func (permission Permission) CheckRoleHavePermission(role Role) bool {
	permissionRoles := permission.Roles
	for i := range permissionRoles {
		if role.ID == permissionRoles[i].ID {
			return true
		}
	}
	return false
}
