package permission

import (
	"api/libs/helpers"
	"encoding/json"
)

// UserAccess Data structure for User Access
// This is stored as Role/Permission on Users table
type UserAccess struct {
	PermissionName string `json:"p"`
	Level          string `json:"l"`
	ReferenceID    int64  `json:"r"`
}

// CompanyLevel ...
const CompanyLevel = "c"

// SiteLevel ...
const SiteLevel = "s"

// ProductLevel ...
const ProductLevel = "p"

// GetUserAccesses Get all user accesses
func GetUserAccesses(permissionString string) (ua []UserAccess, err error) {
	if permissionString != "" {
		_, err = helpers.StringToInterface(permissionString, &ua, helpers.StringTypeJSON)
		if err != nil {
			return
		}
	}
	return
}

// GetUserAccess Get user access depending on filters provided
func GetUserAccess(permissionString string,
	name string, level string, referenceID int64) (ups UserAccess, err error) {
	var userAccesses []UserAccess
	if permissionString != "" {
		_, err = helpers.StringToInterface(permissionString, &userAccesses, helpers.StringTypeJSON)
		if err != nil {
			return
		}
	}
	for i := range userAccesses {
		if userAccesses[i].PermissionName == SuperAdmin.ID {
			ups = userAccesses[i]
			return
		}
		if userAccesses[i].Level == level &&
			userAccesses[i].PermissionName == name &&
			userAccesses[i].ReferenceID == referenceID {
			ups = userAccesses[i]
			return
		}
	}
	return
}

// AddUserAccess Adds User access
func AddUserAccess(userAccess string,
	name string, level string, referenceID int64) (ua []UserAccess, err error) {
	var ups UserAccess
	if userAccess != "" {
		_, err = helpers.StringToInterface(userAccess, &ua, helpers.StringTypeJSON)
		ups, err = GetUserAccess(userAccess, name, level, referenceID)
		if err != nil {
			return
		}
	}
	if (ups == UserAccess{}) {
		newUserPermission := UserAccess{
			PermissionName: name,
			Level:          level,
			ReferenceID:    referenceID,
		}
		ua = append(ua, newUserPermission)
	}
	return
}

// EncodeUserAccess Encodes user access to jSON string
func EncodeUserAccess(ua []UserAccess) (str string, err error) {
	if len(ua) == 0 {
		return "", nil
	}
	var b []byte
	b, err = json.Marshal(ua)
	if err != nil {
		return
	}
	str = (string)(b)
	return
}
