package permission

// Role User Role data structure
type Role struct {
	ID          string
	Name        string
	Description string
	Parent      []Role
}

// SuperAdmin Highest possible role
var SuperAdmin = Role{
	ID:          "SA",
	Name:        "Super Admin",
	Description: "Super Admin Role",
}

// Admin Is not restricted to any site or company context
var Admin = Role{
	ID:          "A",
	Name:        "Admin",
	Description: "Admin Role",
	Parent: []Role{
		SuperAdmin,
	},
}

// Owner for specific company
var Owner = Role{
	ID:          "O",
	Name:        "Owner",
	Description: "Owner of the Company",
	Parent: []Role{
		SuperAdmin,
	},
}

// Manager for specific company
var Manager = Role{
	ID:          "M",
	Name:        "Manager",
	Description: "Manager of the Company",
	Parent: []Role{
		Admin,
		Owner,
	},
}

// Staff ...
var Staff = Role{
	ID:          "S",
	Name:        "Staff",
	Description: "Staff are users registered with company",
	Parent: []Role{
		Manager,
	},
}

// RegisteredUser ...
var RegisteredUser = Role{
	ID:          "R",
	Name:        "Registered User",
	Description: "Registered User are the users who has created an account",
	Parent: []Role{
		Staff,
	},
}

// Guest ...
var Guest = Role{
	ID:          "G",
	Name:        "Guest",
	Description: "Anyone",
	Parent: []Role{
		RegisteredUser,
	},
}

// Roles List of available roles
var Roles = []Role{
	SuperAdmin,
	Admin,
	Owner,
	Manager,
	Staff,
	RegisteredUser,
	Guest,
}

// GetRoleByID Gets role by ID
func GetRoleByID(roleID string) (Role, int) {
	var role Role
	found := 0
	for i := range Roles {
		if Roles[i].ID == roleID {
			role = Roles[i]
			found++
		}
	}
	return role, found
}
