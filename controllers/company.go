package controllers

import (
	"api/libs/controllers"
	"api/libs/database"
	"api/libs/permission"
	"api/libs/swagger"
	"api/models"

	"github.com/emicklei/go-restful"
)

// CompanyResource Endpoint for company resources and its childs
type CompanyResource struct {
	RestController *controllers.RestController
}

// Register CompanyResource and its children endpoints
func (cr CompanyResource) Register(container *restful.Container) {
	ws := new(restful.WebService)
	dh := swagger.DocHelper{
		Model:        models.Company{},
		ResourceName: "companies",
	}

	dh.ControllerDefinition(ws)
	dh.GetAll(ws.GET("/").To(cr.GetAll), ws, true)
	dh.Get(ws.GET("/{company-id}").To(cr.Get), ws, true)
	dh.Create(ws.POST("/").To(cr.Create), ws, true)
	dh.Update(ws.PUT("/{company-id}").To(cr.Update), ws, true)
	dh.Delete(ws.DELETE("/{company-id}").To(cr.Delete), ws, true)

	// Site Resources
	var siteResource SiteResource
	dh.ResourceName = "sites"
	dh.Model = models.Site{}
	dh.GetAll(ws.GET("/{company-id}/sites/").To(siteResource.GetAll), ws, true)
	dh.Get(ws.GET("/{company-id}/sites/{site-id}").To(siteResource.Get), ws, true)
	dh.Create(ws.POST("/{company-id}/sites/").To(siteResource.Create), ws, true)
	dh.Update(ws.PUT("/{company-id}/sites/{site-id}").To(siteResource.Update), ws, true)
	dh.Delete(ws.DELETE("/{company-id}/sites/{site-id}").To(siteResource.Delete), ws, true)

	container.Add(ws)
}

func (cr *CompanyResource) init(request *restful.Request, response *restful.Response) (err restful.ServiceError) {
	cr.RestController = new(controllers.RestController)
	cr.RestController.ControllerModel = &controllers.ControllerModel{
		Model: new(models.Company),
	}
	cr.RestController.Init(request, response)
	return
}

// Get ...
func (cr CompanyResource) Get(request *restful.Request, response *restful.Response) {
	err := cr.init(request, response)
	if (err != restful.ServiceError{}) {
		return
	}
	cr.RestController.Get(request, response)
	if (cr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(200, cr.RestController.ControllerModel.Model)
	}
}

// GetAll ...
func (cr CompanyResource) GetAll(request *restful.Request, response *restful.Response) {
	err := cr.init(request, response)
	if (err != restful.ServiceError{}) {
		return
	}
	cr.RestController.GetAll(request, response)
	if (cr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		modelList := []models.Company{}
		database.DB.Find(&modelList)
		response.WriteHeaderAndEntity(200, modelList)
	}
}

// Create ...
func (cr CompanyResource) Create(request *restful.Request, response *restful.Response) {
	err := cr.init(request, response)
	if (err != restful.ServiceError{}) {
		return
	}
	per := models.Company{}.GetPermissions()[permission.ActionCreate]
	userHasPermission := cr.RestController.CheckUserHasCompanyPermission(request, response, per, nil)
	if !userHasPermission {
		return
	}
	cr.RestController.Create(request, response)
	if (cr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(201, cr.RestController.ControllerModel.Model)
	}
}

// Update ...
func (cr CompanyResource) Update(request *restful.Request, response *restful.Response) {
	err := cr.init(request, response)
	if (err != restful.ServiceError{}) {
		return
	}
	per := models.Company{}.GetPermissions()[permission.ActionUpdate]
	if cr.RestController.GetModel(request) {
		return
	}
	userHasPermission := cr.RestController.CheckUserHasCompanyPermission(request, response, per, cr.RestController.ControllerModel.Model.(*models.Company))
	if !userHasPermission {
		return
	}
	cr.RestController.Update(request, response)
	if (cr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(200, cr.RestController.ControllerModel.Model)
	}
}

// Delete ...
func (cr CompanyResource) Delete(request *restful.Request, response *restful.Response) {
	err := cr.init(request, response)
	if (err != restful.ServiceError{}) {
		return
	}
	per := models.Company{}.GetPermissions()[permission.ActionDelete]
	userHasPermission := cr.RestController.CheckUserHasCompanyPermission(request, response, per, cr.RestController.ControllerModel.Company)
	if !userHasPermission {
		return
	}
	cr.RestController.Delete(request, response)
	if (cr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(204, "")
	}
}
