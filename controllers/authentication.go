package controllers

import (
	"api/libs/database"
	"api/libs/permission"
	"api/models"
	"api/services"

	"github.com/RangelReale/osin"
	"github.com/emicklei/go-restful"
)

// AuthenticationResource operations for Authentication
type AuthenticationResource struct {
	server *osin.Server
}

// Register endpoints for Authentication
func (a AuthenticationResource) Register(container *restful.Container) {
	ws := new(restful.WebService)
	ws.
		Path("/auth").
		Doc("Authenticate Users").
		Consumes("application/x-www-form-urlencoded", "multipart/form-data").
		Produces(restful.MIME_JSON, restful.MIME_XML) // you can specify this per route as well

	ws.Route(ws.GET("/authorize").To(a.Authorize).
		// docs
		Doc("Authorize user. Not Implemented yet").
		Operation("authorizeUser"))

	ws.Route(ws.POST("/token").To(a.Token).
		// docs
		Doc("Get Access Token").
		Operation("accessToken"))

	ws.Route(ws.GET("/info").To(a.Info).
		// docs
		Doc("Information about user").
		Operation("userInfo"))

	container.Add(ws)
}

// Prepare will set the defaults values for AuthenticationResource
func (a *AuthenticationResource) Prepare() {
	sConfig := osin.NewServerConfig()
	sConfig.AllowedAuthorizeTypes = osin.AllowedAuthorizeType{osin.CODE, osin.TOKEN}
	sConfig.AllowedAccessTypes = osin.AllowedAccessType{osin.AUTHORIZATION_CODE,
		osin.REFRESH_TOKEN, osin.PASSWORD, osin.CLIENT_CREDENTIALS, osin.ASSERTION}
	sConfig.AllowGetAccessRequest = true
	sConfig.AllowClientSecretInParams = true
	sConfig.AccessExpiration = 60 * 60 * 24
	a.server = osin.NewServer(sConfig, models.NewAuthStorage())
}

// Authorize ...
// @Title Post
// @Description authorize
// @Param	body		body 	models.AuthStorage	true		"body for Companies content"
// @Success 201 {int}models.Company
// @Failure 403 body is empty
// @router /authorize [get]
func (a *AuthenticationResource) Authorize(request *restful.Request, response *restful.Response) {
	a.Prepare()
	// TestStorage implements the "osin.Storage" interface
	server := osin.NewServer(osin.NewServerConfig(), &models.AuthStorage{})
	resp := server.NewResponse()
	defer resp.Close()

	if ar := server.HandleAuthorizeRequest(resp, request.Request); ar != nil {

		// HANDLE LOGIN PAGE HERE

		ar.Authorized = true
		server.FinishAuthorizeRequest(resp, request.Request, ar)
	}
	response.WriteAsJson(resp.Output)
}

// Token provided the accessToken based on credentials provided
// @Title Post
// @Description authorize
// @Param	body		body 	models.AuthStorage	true		"body for Companies content"
// @Success 201 {int}models.Company
// @Failure 403 body is empty
// @router /token [post]
func (a *AuthenticationResource) Token(request *restful.Request, response *restful.Response) {
	a.Prepare()
	resp := a.server.NewResponse()
	defer resp.Close()

	if ar := a.server.HandleAccessRequest(resp, request.Request); ar != nil {
		switch ar.Type {
		case osin.AUTHORIZATION_CODE:
			ar.Authorized = false
			break
		case osin.REFRESH_TOKEN:
			ar.Authorized = false
			break
		case osin.PASSWORD:
			user, err := services.UserService{}.ValidatePassword(ar.Username, ar.Password)
			if err != nil {
				ar.Authorized = false
			} else {
				ar.UserData = user
				ar.Authorized = true
			}
			break
		case osin.CLIENT_CREDENTIALS:
			ar.Authorized = false
			break
		case osin.ASSERTION:
			if ar.AssertionType == "urn:osin.example.complete" && ar.Assertion == "osin.data" {
				ar.Authorized = true
			}
			break
		}
		a.server.FinishAccessRequest(resp, request.Request, ar)
	}
	response.WriteAsJson(resp.Output)
	//spew.Dump(resp)
}

// Info provides the logged in user information via accessToken provided
// @Title Get
// @Description info
// @Param	body		body 	models.AuthStorage	true		"body for Companies content"
// @Success 201 {int}models.Company
// @Failure 403 body is empty
// @router /info [get]
func (a *AuthenticationResource) Info(request *restful.Request, response *restful.Response) {
	a.Prepare()
	// TestStorage implements the "osin.Storage" interface
	server := osin.NewServer(osin.NewServerConfig(), &models.AuthStorage{})
	resp := server.NewResponse()
	defer resp.Close()

	if ir := server.HandleInfoRequest(resp, request.Request); ir != nil {
		var person models.Person
		var user models.User
		userID := ir.AccessData.UserData.(int64)
		database.DB.First(&user, userID)
		database.DB.First(&person, user.PersonID)
		resp.Output["person"] = person
		resp.Output["user"] = user
		resp.Output["roles"] = user.GetRoles(permission.CompanyLevel, 0)
		server.FinishInfoRequest(resp, request.Request, ir)
	}
	response.WriteAsJson(resp.Output)
}
