package controllers

import (
	"api/libs/controllers"
	"api/libs/database"
	"api/libs/swagger"
	"api/models"

	"github.com/emicklei/go-restful"
)

// SiteResource Endpoint for Site and its child resources
type SiteResource struct {
	RestController *controllers.RestController
}

// Register Endpoint
func (sr SiteResource) Register(container *restful.Container) {
	ws := new(restful.WebService)
	dh := swagger.DocHelper{
		Model:        models.Site{},
		ResourceName: "sites",
	}

	dh.ControllerDefinition(ws)

	container.Add(ws)
}

func (sr *SiteResource) init() {
	sr.RestController = new(controllers.RestController)
	sr.RestController.ControllerModel = &controllers.ControllerModel{
		Model:            new(models.Site),
		IsCompanyContext: true,
	}
}

// Get ...
func (sr SiteResource) Get(request *restful.Request, response *restful.Response) {
	sr.init()
	sr.RestController.Get(request, response)
	if (sr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(200, sr.RestController.ControllerModel.Model)
	}
}

// GetAll ...
func (sr SiteResource) GetAll(request *restful.Request, response *restful.Response) {
	sr.init()
	sr.RestController.GetAll(request, response)
	if (sr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		modelList := []models.Site{}
		// TODO: Write generic code for `Where` depending on context
		database.DB.Where("company_id = ? ", sr.RestController.ControllerModel.Company.ID).Find(&modelList)
		response.WriteHeaderAndEntity(200, modelList)
	}
}

// Create ...
func (sr SiteResource) Create(request *restful.Request, response *restful.Response) {
	sr.init()
	sr.RestController.Create(request, response)
	if (sr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(201, sr.RestController.ControllerModel.Model)
	}
}

// Update ...
func (sr SiteResource) Update(request *restful.Request, response *restful.Response) {
	sr.init()
	sr.RestController.Update(request, response)
	if (sr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(200, sr.RestController.ControllerModel.Model)
	}
}

// Delete ...
func (sr SiteResource) Delete(request *restful.Request, response *restful.Response) {
	sr.init()
	sr.RestController.Delete(request, response)
	if (sr.RestController.ControllerModel.ServiceError == restful.ServiceError{}) {
		response.WriteHeaderAndEntity(204, "")
	}
}
