package services

import (
	"golang.org/x/crypto/bcrypt"
	"api/libs/database"
	"api/models"
	"errors"
)

// UserService ...
type UserService struct{}

// ValidatePassword ...
func (us UserService) ValidatePassword(username string, password string) (user models.User, err error) {
	recordNotFound := database.DB.Where("username = ?", username).First(&user).RecordNotFound()
	if recordNotFound {
		err = errors.New("User does not exists")
		return
	}
	password = password + user.Seed
	err = bcrypt.CompareHashAndPassword([]byte (user.Password), []byte (password))
	if err != nil {
		err = errors.New("Invalid password")
		return
	}
	return user, nil
}