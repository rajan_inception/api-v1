package command

import (
	"api/libs/helpers"
	"api/libs/permission"
	"api/models"

	"github.com/jinzhu/gorm"
)

// CreateClient creates default client
func CreateClient(db *gorm.DB) {
	client := models.Client{
		ID:          196039034550976513,
		Secret:      "661fad2bc9c36c458c2f87788fce5697",
		RedirectURI: "http://localhost",
	}
	db.Create(&client)
}

// CreateUser creates list of basic users
func CreateUser(db *gorm.DB) {
	users := []models.User{
		{
			Username: "ir.super.admin@yopmail.com",
			Password: "super_admin",
			Roles:    `[{"p":"SA","l":"c","r":0}]`,
		},
		{
			Username: "ir.admin@yopmail.com",
			Password: "admin",
			Roles:    `[{"p":"A","l":"c","r":0}]`,
		},
		{
			Username: "ir.manager@yopmail.com",
			Password: "manager",
		},
		{
			Username: "ir.owner@yopmail.com",
			Password: "owner",
		},
		{
			Username: "ir.staff@yopmail.com",
			Password: "staff",
		},
		{
			Username: "ir.staff2@yopmail.com",
			Password: "staff2",
		},
		{
			Username: "ir.reg@yopmail.com",
			Password: "reg",
		},
		{
			Username: "ir.reg2@yopmail.com",
			Password: "reg2",
		},
	}

	for _, user := range users {
		password := user.Password
		person := models.Person{
			Contact: &models.Contact{
				FirstName: password,
			},
			User: &user,
		}
		db.Create(&person)
	}
}

// CreateCompany creates demo company
func CreateCompany(db *gorm.DB, owner models.Person) models.Company {
	company := models.Company{
		Name:    "Thai",
		OwnerID: owner.ID,
		Address: new(models.Address),
	}
	company.CurrentPersonID = owner.ID
	db.Create(&company)
	user, _ := owner.GetUser()
	user.AddRole(permission.Owner.ID, permission.CompanyLevel, company.ID)
	return company
}

// CreateSite creates demo site
func CreateSite(db *gorm.DB, company models.Company, owner models.Person) models.Site {
	site := models.Site{
		Name:      "Sydney Thai",
		CompanyID: company.ID,
	}
	site.CurrentPersonID = owner.ID
	db.Create(&site)
	return site
}

// CreateProduct creates a single Product from given JSON, XML or YML string
func CreateProduct(db *gorm.DB, site models.Site, attributes string) *models.Product {
	product := new(models.Product)
	product.SiteID = site.ID
	db.LogMode(true)
	helpers.StringToInterface(attributes, product, helpers.StringTypeJSON)
	db.Create(&product)
	return product
}

// CreateProducts creates products from the batch data provided
func CreateProducts(db *gorm.DB, site models.Site, attributes string) []models.Product {
	var products []models.Product
	helpers.StringToInterface(attributes, &products, helpers.StringTypeJSON)
	for _, product := range products {
		product.SiteID = site.ID
		db.Create(&product)
	}
	return products
}
