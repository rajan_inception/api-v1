package main

import (
	_ "github.com/lib/pq"
	"api/tests/setup"
	"api/libs/database"
	"api/libs/system"
	"os"
)

func main() {
	os.Setenv(system.Env, system.EnvDebug)
	database.DB, _= database.GetDB()
	setup.PopulateData("../api/datas")
	//command.CreateClient(db);
	//command.CreateUser(db);
}