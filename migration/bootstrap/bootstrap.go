package bootstrap

import "github.com/jinzhu/gorm"

// Create new table for DB
func Create(Db *gorm.DB) {
	// Create Tables for Oauth
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS oauth_clients (
		id SERIAL    PRIMARY KEY,
		secret       STRING(32) NOT NULL,
		extra        STRING  NULL,
		redirect_uri STRING NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL
	)`)

	Db.Exec(`
		CREATE TABLE IF NOT EXISTS oauth_authorize (
		code         STRING NOT NULL PRIMARY KEY,
		client_id    INT NOT NULL,
		expires_in   INT NOT NULL,
		scope        STRING NOT NULL,
		redirect_uri STRING NULL,
		state        STRING NOT NULL,
		extra        STRING  NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL
	)`)

	Db.Exec(`
		CREATE TABLE IF NOT EXISTS oauth_accesses (
		access_token       STRING(32) NOT NULL PRIMARY KEY,
		client_id          INT NOT NULL,
		user_id      INT NOT NULL,
		authorize_code     STRING NOT NULL,
		previous           STRING NOT NULL,
		refresh_token      STRING(32) NOT NULL,
		expires_in         INT NOT NULL,
		scope              STRING NULL,
		redirect_uri       STRING  NULL,
		extra 		   STRING  NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL
	)`)

	Db.Exec(`
		CREATE TABLE IF NOT EXISTS oauth_refresh (
		token         STRING(32) NOT NULL PRIMARY KEY,
		access        STRING NOT NULL
	)`)

	// Create Table Taxes
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS taxes (
		id SERIAL ,
		name STRING(20) NOT NULL,
		description STRING NULL,
		rate FLOAT NOT NULL,
		code STRING(5) NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_taxes_deleted_at (deleted_at)
	)`)

	// Create Table Address
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS addresses (
		id SERIAL ,
		house_number STRING(5) NULL,
		unit_number STRING(3) NULL,
		street_address STRING NULL,
		city STRING(40) NULL,
		state STRING(30) NULL,
		region STRING(30) NULL,
		country STRING(30) NULL,
		post_code STRING(10) NULL,
		latitude FLOAT NULL,
		longitude FLOAT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_addresses_deleted_at (deleted_at)
        )`)

	// Create Table Currencies
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS currencies (
		id SERIAL ,
		name STRING(30) NOT NULL,
		sign STRING(3) NULL,
		portable STRING(6) NULL,
		space BOOL DEFAULT FALSE,
		prepend BOOL DEFAULT TRUE,
		precesion INT NOT NULL DEFAULT 2,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_currencies_deleted_at (deleted_at)
	)`)

	// Create Table Rates
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS rates (
		id SERIAL ,
		name STRING(10) NOT NULL,
		min INT NOT NULL,
		max INT NOT NULL,
		amount FLOAT NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_plans_deleted_at (deleted_at)
	)`)

	// Create Table Media
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS media (
		id SERIAL ,
		type STRING(15) NOT NULL,
		name STRING(30) NOT NULL,
		url STRING NULL,
		extension STRING(5) NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_media_deleted_at (deleted_at)
	)`)

	// Create Table Contacts
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS contacts (
		id SERIAL ,
		first_name STRING(20) NOT NULL,
		middle_name STRING(20) NULL,
		last_name STRING(20) NOT NULL,
		website STRING(30) NULL,
		note STRING NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_contacts_deleted_at (deleted_at)
	)`)

	// Create Table Emails
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS emails (
		id SERIAL ,
		email_address STRING(50) NOT NULL,
		contact_id INT NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_emails_deleted_at (deleted_at),
		INDEX idx_emails_contact_id (contact_id),
		CONSTRAINT fk_contacts_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id)
	)`)

	// Create Table Faxes
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS faxes (
		id SERIAL ,
		number STRING(20) NOT NULL,
		contact_id INT,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_faxes_deleted_at (deleted_at),
		INDEX idx_faxes_contact_id (contact_id),
		CONSTRAINT fk_faxes_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id)
	)`)

	// Create Table Phones
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS phones (
		id SERIAL ,
		type STRING(10) NULL,
		number STRING(2) NULL,
		contact_id INT,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_phones_deleted_at (deleted_at),
		INDEX idx_phones_contact_id (contact_id),
		CONSTRAINT fk_phones_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id)
	)`)

	// Create Table Media Contacts
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS media_contacts (
		media_id INT NOT NULL,
		contact_id INT NOT NULL,
		INDEX idx_media_contacts (media_id, contact_id)
	)`)

	// Create Table People
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS people (
		id SERIAL ,
		contact_id INT NULL,
		address_id INT NULL,
		active_since TIMESTAMP WITH TIME ZONE NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_people_deleted_at (deleted_at),
		INDEX idx_people_contact_id (contact_id),
		INDEX idx_people_address_id (address_id),
		CONSTRAINT fk_people_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id),
		CONSTRAINT fk_people_address_id FOREIGN KEY (address_id) REFERENCES addresses (id)
	)`)

	// Create Table Users
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS users (
		id SERIAL ,
		username STRING(50) NULL,
		password STRING NULL,
		person_id INT NULL,
		roles STRING,
		permissions STRING,
		seed STRING(16) NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_users_deleted_at (deleted_at),
		INDEX idx_people_user_id (person_id),
		CONSTRAINT fk_user_person_id FOREIGN KEY (person_id) REFERENCES people (id)
	)`)

	// Create Table Subscriptions
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS subscriptions (
		id SERIAL ,
		payment_method STRING(10) NULL,
		billing_cycle STRING(10) NULL,
		billing_day STRING(10) NULL,
		status STRING(10) NULL,
		discount FLOAT NULL,
		is_discount_percentage BOOL DEFAULT TRUE,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_subscriptions_deleted_at (deleted_at)
	)`)

	// Create Table Companies
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS companies (
		id SERIAL ,
		name STRING NOT NULL,
		company_code STRING NULL,
		abn STRING NULL,
		description STRING NULL,
		verified_on TIMESTAMP WITH TIME ZONE NULL,
		subscription_id INT NULL,
		address_id INT NULL,
		owner_id INT NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_companies_deleted_at (deleted_at),
		INDEX idx_companies_subscription_id (subscription_id),
		CONSTRAINT fk_companies_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscriptions (id),
		INDEX idx_companies_address_id (address_id),
		CONSTRAINT fk_companies_address_id FOREIGN KEY (address_id) REFERENCES addresses (id),
		INDEX idx_companies_owner_id (owner_id),
		CONSTRAINT fk_companies_owner_id FOREIGN KEY (owner_id) REFERENCES people (id)
        )`)

	// Create Table Sites
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS sites (
		id SERIAL ,
		name STRING(30) NOT NULL,
		code STRING(8) NULL,
		description STRING NULL,
		timezone STRING(30) NULL,
		company_id INT NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_sites_deleted_at (deleted_at),
		INDEX idx_sites_company_id (company_id),
		CONSTRAINT fk_sites_company_id FOREIGN KEY (company_id) REFERENCES companies (id)
	)`)

	// Create Table Products
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS products (
		id SERIAL ,
		site_id INT NOT NULL,
		name STRING(100) NOT NULL,
		code STRING(40) NOT NULL,
		description STRING NULL,
		barcode STRING(20) NULL,
		cost_ex_tax FLOAT NULL,
		cost_tax FLOAT NULL,
		is_for_sale BOOL DEFAULT TRUE,
		is_modifier BOOL DEFAULT FALSE,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_products_deleted_at (deleted_at),
		INDEX idx_products_site_id (site_id),
		CONSTRAINT fk_products_site_id FOREIGN KEY (site_id) REFERENCES sites (id)
	)`)

	// Create Table Product_variants
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS product_variants (
		id SERIAL ,
		product_id INT NOT NULL,
		variant_id INT NOT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_product_variants_deleted_at (deleted_at),
		INDEX idx_product_variants_product_id (product_id),
		INDEX idx_product_variants_variant_id (variant_id),
		CONSTRAINT fk_product_variants_product_id FOREIGN KEY (product_id) REFERENCES products (id),
		CONSTRAINT fk_product_variants_variant_id FOREIGN KEY (variant_id) REFERENCES products (id)
	)`)

	// Create Table Product_modifiers
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS product_modifiers (
		product_id INT NOT NULL,
		modifier_id INT NOT NULL,
		cost_ex_tax FLOAT NULL,
		cost_tax FLOAT NULL,
		mandatory BOOL DEFAULT FALSE,
		alternatives STRING,
		CONSTRAINT "primary" PRIMARY KEY (product_id, modifier_id),
		INDEX idx_product_modifiers_product_id (product_id),
		INDEX idx_product_modifiers_modifier_id (modifier_id),
		CONSTRAINT fk_product_modifiers_product_id FOREIGN KEY (product_id) REFERENCES products (id),
		CONSTRAINT fk_product_modifiers_variant_id FOREIGN KEY (modifier_id) REFERENCES products (id)
	)`)

	// Create Table Tables
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS tables (
		id SERIAL ,
		site_id INT NOT NULL,
		name STRING(30) NOT NULL,
		code STRING(10) NULL,
		barcode STRING NOT NULL,
		section_name STRING(20) NULL,
		sort_order INT NULL,
		available BOOL DEFAULT TRUE,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_tables_deleted_at (deleted_at),
		INDEX idx_tables_site_id (site_id),
		CONSTRAINT fk_tables_site_id FOREIGN KEY (site_id) REFERENCES sites (id)
	)`)

	// Create Table Orders
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS orders (
		id SERIAL ,
		site_id INT NOT NULL,
		voided BOOL DEFAULT FALSE,
		notes STRING NULL,
		status STRING(10) NULL,
		placed_at TIMESTAMP WITH TIME ZONE NULL,
		completed_at TIMESTAMP WITH TIME ZONE NULL,
		total_ex_tax FLOAT DEFAULT 0,
		total_tax FLOAT DEFAULT 0,
		discount FLOAT DEFAULT 0,
		is_discount_percentage BOOL DEFAULT TRUE,
		chat_history STRING NULL,
		estimated_time INT NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_orders_deleted_at (deleted_at),
		INDEX idx_orders_site_id (site_id),
		CONSTRAINT fk_orders_site_id FOREIGN KEY (site_id) REFERENCES sites (id)
	)`)

	// Create Table Invoices
	Db.Exec(`
		CREATE TABLE IF NOT EXISTS invoices (
		id SERIAL ,
		company_id INT NOT NULL,
		site_id INT NOT NULL,
		code STRING(10) NULL,
		date TIMESTAMP WITH TIME ZONE NOT NULL,
		start_date TIMESTAMP WITH TIME ZONE NULL,
		amount_ex_tax FLOAT NULL,
		amount_tax FLOAT NULL,
		credit_amount FLOAT NULL,
		status STRING(10) NULL,
		description STRING NULL,
		processed_at TIMESTAMP WITH TIME ZONE NULL,
		content STRING NULL,
		created_at TIMESTAMP WITH TIME ZONE NOT NULL,
		updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
		deleted_at TIMESTAMP WITH TIME ZONE NULL,
		CONSTRAINT "primary" PRIMARY KEY (id),
		INDEX idx_invoices_deleted_at (deleted_at),
		INDEX idx_invoices_site_id (site_id),
		CONSTRAINT fk_invoices_site_id FOREIGN KEY (site_id) REFERENCES sites (id),
		INDEX idx_invoices_company_id (company_id),
		CONSTRAINT fk_invoices_company_id FOREIGN KEY (company_id) REFERENCES companies (id)
	)`)
}

// Drop all existing tables
func Drop(Db *gorm.DB) {

	Db.Exec("DROP TABLE IF EXISTS  invoices")

	// Table for Company and Site Stuffs
	Db.Exec("DROP TABLE IF EXISTS  orders")
	Db.Exec("DROP TABLE IF EXISTS  tables")
	Db.Exec("DROP TABLE IF EXISTS  product_modifiers")
	Db.Exec("DROP TABLE IF EXISTS  product_variants")
	Db.Exec("DROP TABLE IF EXISTS  products")
	Db.Exec("DROP TABLE IF EXISTS  sites")
	Db.Exec("DROP TABLE IF EXISTS  companies")

	// Table for Subscription
	Db.Exec("DROP TABLE IF EXISTS  subscriptions")

	// Users Details
	Db.Exec("DROP TABLE IF EXISTS  users")
	Db.Exec("DROP TABLE IF EXISTS  people")

	// Tables for Contact
	Db.Exec("DROP TABLE IF EXISTS  media_contacts")
	Db.Exec("DROP TABLE IF EXISTS  phones")
	Db.Exec("DROP TABLE IF EXISTS  faxes")
	Db.Exec("DROP TABLE IF EXISTS  emails")
	Db.Exec("DROP TABLE IF EXISTS  contacts")

	// Independent Tables
	Db.Exec("DROP TABLE IF EXISTS  media")
	Db.Exec("DROP TABLE IF EXISTS  rates")
	Db.Exec("DROP TABLE IF EXISTS  currencies")
	Db.Exec("DROP TABLE IF EXISTS  addresses")
	Db.Exec("DROP TABLE IF EXISTS  taxes")

	Db.Exec("DROP TABLE IF EXISTS  oauth_refresh")
	Db.Exec("DROP TABLE IF EXISTS  oauth_accesses")
	Db.Exec("DROP TABLE IF EXISTS  oauth_authorize")
	Db.Exec("DROP TABLE IF EXISTS  oauth_clients")
}
