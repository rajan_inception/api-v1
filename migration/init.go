package main

import (
	_ "github.com/lib/pq"
	"github.com/jinzhu/gorm"
	"api/libs/database"
	"api/migration/bootstrap"
	"api/libs/system"
	"os"
	"api/tests/setup"
)

func main() {
	os.Setenv(system.Env, system.EnvDebug)
	Db, _ := database.GetDB()
	bootstrap.Drop(Db);
	bootstrap.Create(Db);
	defer testData(Db)
}

func testData(Db *gorm.DB) {
	database.DB = Db
	setup.PopulateData("../api/datas")

	type X struct {
		Id int64
		Contacts []X `gorm:"associationforeignkey:nonExistentField;many2many:x_y;"`
	}
	Db.AutoMigrate(&X{})
}

