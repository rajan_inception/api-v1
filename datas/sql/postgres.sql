CREATE TABLE oauth_clients (
	ID SERIAL PRIMARY KEY,
	secret VARCHAR (32) NOT NULL,
	extra TEXT NULL,
	redirect_uri TEXT NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE oauth_authorize (
	code TEXT NOT NULL PRIMARY KEY,
	client_id INT NOT NULL,
	expires_in INT NOT NULL,
	SCOPE TEXT NOT NULL,
	redirect_uri TEXT NULL,
	STATE TEXT NOT NULL,
	extra TEXT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE oauth_accesses (
	access_token VARCHAR (32) NOT NULL PRIMARY KEY,
	client_id INT NOT NULL,
	user_id INT NOT NULL,
	authorize_code TEXT NOT NULL,
	previous TEXT NOT NULL,
	refresh_token VARCHAR (32) NOT NULL,
	expires_in INT NOT NULL,
	SCOPE TEXT NULL,
	redirect_uri TEXT NULL,
	extra TEXT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE oauth_refresh (
	token VARCHAR (32) NOT NULL PRIMARY KEY,
	ACCESS TEXT NOT NULL
);

CREATE TABLE taxes (
	ID SERIAL PRIMARY KEY,
	NAME VARCHAR (20) NOT NULL,
	description TEXT NULL,
	rate FLOAT NOT NULL,
	code VARCHAR (5) NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE locations (
	ID SERIAL PRIMARY KEY,
	house_number VARCHAR (5) NULL,
	unit_number VARCHAR (3) NULL,
	street_location TEXT NULL,
	city VARCHAR (40) NULL,
	STATE VARCHAR (30) NULL,
	region VARCHAR (30) NULL,
	country VARCHAR (30) NULL,
	post_code VARCHAR (10) NULL,
	latitude FLOAT NULL,
	longitude FLOAT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE currencies (
	ID SERIAL PRIMARY KEY,
	NAME VARCHAR (30) NOT NULL,
	sign VARCHAR (3) NULL,
	portable VARCHAR (6) NULL,
	SPACE BOOL DEFAULT FALSE,
	prepend BOOL DEFAULT TRUE,
	precesion INT NOT NULL DEFAULT 2,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE rates (
	ID SERIAL PRIMARY KEY,
	NAME VARCHAR (10) NOT NULL,
	MIN INT NOT NULL,
	MAX INT NOT NULL,
	amount FLOAT NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE media (
	ID SERIAL PRIMARY KEY,
	TYPE VARCHAR (15) NOT NULL,
	NAME VARCHAR (30) NOT NULL,
	url TEXT NULL,
	extension VARCHAR (5) NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE contacts (
	ID SERIAL PRIMARY KEY,
	first_name VARCHAR (20) NOT NULL,
	middle_name VARCHAR (20) NULL,
	last_name VARCHAR (20) NOT NULL,
	website VARCHAR (30) NULL,
	note TEXT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE emails (
	ID SERIAL PRIMARY KEY,
	email_location VARCHAR (50) NOT NULL,
	contact_id INT NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_contacts_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (ID)
);

CREATE TABLE faxes (
	ID SERIAL PRIMARY KEY,
	NUMBER VARCHAR (20) NOT NULL,
	contact_id INT,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_faxes_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (ID)
);

CREATE TABLE phones (
	ID SERIAL PRIMARY KEY,
	TYPE VARCHAR (10) NULL,
	NUMBER VARCHAR (2) NULL,
	contact_id INT,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_phones_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (ID)
);

CREATE TABLE media_contacts (
	media_id INT NOT NULL,
	contact_id INT NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (media_id, contact_id)
);

CREATE TABLE people (
	ID SERIAL PRIMARY KEY,
	contact_id INT NULL,
	location_id INT NULL,
	active_since TIMESTAMP WITH TIME ZONE NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_people_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (ID),
	CONSTRAINT fk_people_location_id FOREIGN KEY (location_id) REFERENCES locations (ID)
);

CREATE TABLE users (
	ID SERIAL PRIMARY KEY,
	username VARCHAR (50) NULL,
	PASSWORD TEXT NULL,
	person_id INT NULL,
	roles JSON,
	permissions JSON,
	seed VARCHAR (16) NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_user_person_id FOREIGN KEY (person_id) REFERENCES people (ID)
);

CREATE TABLE subscriptions (
	ID SERIAL PRIMARY KEY,
	payment_method VARCHAR (10) NULL,
	billing_cycle VARCHAR (10) NULL,
	billing_day VARCHAR (10) NULL,
	status VARCHAR (10) NULL,
	discount FLOAT NULL,
	is_discount_percentage BOOL DEFAULT TRUE,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL
);

CREATE TABLE companies (
	ID SERIAL PRIMARY KEY,
	NAME TEXT NOT NULL,
	company_code TEXT NULL,
	abn TEXT NULL,
	description TEXT NULL,
	verified_on TIMESTAMP WITH TIME ZONE NULL,
	subscription_id INT NULL,
	location_id INT NULL,
	owner_id INT NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_companies_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscriptions (ID),
	CONSTRAINT fk_companies_location_id FOREIGN KEY (location_id) REFERENCES locations (ID),
	CONSTRAINT fk_companies_owner_id FOREIGN KEY (owner_id) REFERENCES people (ID)
);

CREATE TABLE sites (
	ID SERIAL PRIMARY KEY,
	NAME VARCHAR (30) NOT NULL,
	code VARCHAR (8) NULL,
	description TEXT NULL,
	timezone VARCHAR (30) NULL,
	company_id INT NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_sites_company_id FOREIGN KEY (company_id) REFERENCES companies (ID)
);

CREATE TABLE products (
	ID SERIAL PRIMARY KEY,
	site_id INT NOT NULL,
	NAME VARCHAR (100) NOT NULL,
	code VARCHAR (40) NOT NULL,
	description TEXT NULL,
	barcode VARCHAR (20) NULL,
	cost_ex_tax FLOAT NULL,
	cost_tax FLOAT NULL,
	is_for_sale BOOL DEFAULT TRUE,
	is_modifier BOOL DEFAULT FALSE,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_products_site_id FOREIGN KEY (site_id) REFERENCES sites (ID)
);

CREATE TABLE product_variants (
	ID SERIAL PRIMARY KEY,
	product_id INT NOT NULL,
	variant_id INT NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_product_variants_product_id FOREIGN KEY (product_id) REFERENCES products (ID),
	CONSTRAINT fk_product_variants_variant_id FOREIGN KEY (variant_id) REFERENCES products (ID)
);

CREATE TABLE product_modifiers (
	product_id INT NOT NULL,
	modifier_id INT NOT NULL,
	cost_ex_tax FLOAT NULL,
	cost_tax FLOAT NULL,
	mandatory BOOL DEFAULT FALSE,
	alternatives JSON,
	PRIMARY KEY (product_id, modifier_id),
	CONSTRAINT fk_product_modifiers_product_id FOREIGN KEY (product_id) REFERENCES products (ID),
	CONSTRAINT fk_product_modifiers_variant_id FOREIGN KEY (modifier_id) REFERENCES products (ID)
);

CREATE TABLE tables (
	ID SERIAL PRIMARY KEY,
	site_id INT NOT NULL,
	NAME VARCHAR (30) NOT NULL,
	code VARCHAR (10) NULL,
	barcode TEXT NOT NULL,
	section_name VARCHAR (20) NULL,
	sort_order INT NULL,
	available BOOL DEFAULT TRUE,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_tables_site_id FOREIGN KEY (site_id) REFERENCES sites (ID)
);

CREATE TABLE orders (
	ID SERIAL PRIMARY KEY,
	site_id INT NOT NULL,
	voided BOOL DEFAULT FALSE,
	notes TEXT NULL,
	status VARCHAR (10) NULL,
	placed_at TIMESTAMP WITH TIME ZONE NULL,
	completed_at TIMESTAMP WITH TIME ZONE NULL,
	total_ex_tax FLOAT DEFAULT 0,
	total_tax FLOAT DEFAULT 0,
	discount FLOAT DEFAULT 0,
	is_discount_percentage BOOL DEFAULT TRUE,
	chat_history TEXT NULL,
	estimated_time INT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_orders_site_id FOREIGN KEY (site_id) REFERENCES sites (ID)
);

CREATE TABLE invoices (
	ID SERIAL PRIMARY KEY,
	company_id INT NOT NULL,
	site_id INT NOT NULL,
	code VARCHAR (10) NULL,
	DATE TIMESTAMP WITH TIME ZONE NOT NULL,
	start_date TIMESTAMP WITH TIME ZONE NULL,
	amount_ex_tax FLOAT NULL,
	amount_tax FLOAT NULL,
	credit_amount FLOAT NULL,
	status VARCHAR (10) NULL,
	description TEXT NULL,
	processed_at TIMESTAMP WITH TIME ZONE NULL,
	CONTENT TEXT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE NULL,
	CONSTRAINT fk_invoices_site_id FOREIGN KEY (site_id) REFERENCES sites (ID),
	CONSTRAINT fk_invoices_company_id FOREIGN KEY (company_id) REFERENCES companies (ID)
);