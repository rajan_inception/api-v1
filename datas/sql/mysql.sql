SET FOREIGN_KEY_CHECKS = 0;
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
  FROM information_schema.tables
  WHERE table_schema = 'test'; -- specify DB name here.

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;


CREATE TABLE IF NOT EXISTS oauth_clients (
		id INT NOT NULL    PRIMARY KEY,
		secret       VARCHAR(32) NOT NULL,
		extra        TEXT NULL,
		redirect_uri TEXT NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL
	);

CREATE TABLE IF NOT EXISTS oauth_authorize (
		`code`       VARCHAR(32) NOT NULL PRIMARY KEY,
		client_id    INT NOT NULL,
		expires_in   INT NOT NULL,
		scope        TEXT NOT NULL,
		redirect_uri TEXT NULL,
		state        TEXT NOT NULL,
		extra        TEXT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL
	);

CREATE TABLE IF NOT EXISTS oauth_accesses (
		access_token       VARCHAR(32) NOT NULL PRIMARY KEY,
		client_id          INT NOT NULL,
		user_id      INT NOT NULL,
		authorize_code     TEXT NOT NULL,
		previous           TEXT NOT NULL,
		refresh_token      VARCHAR(32) NOT NULL,
		expires_in         INT NOT NULL,
		scope              TEXT NULL,
		redirect_uri       TEXT NULL,
		extra 		   TEXT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL
	);

CREATE TABLE IF NOT EXISTS oauth_refresh (
		token         VARCHAR(32) NOT NULL PRIMARY KEY,
		access        TEXT NOT NULL
	);

CREATE TABLE IF NOT EXISTS taxes (
		id INT NOT NULL ,
		name VARCHAR(20) NOT NULL,
		description TEXT NULL,
		rate FLOAT NOT NULL,
		code VARCHAR(5) NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_taxes_deleted_at (deleted_at)
	);

CREATE TABLE IF NOT EXISTS locations (
		id INT NOT NULL ,
		house_number VARCHAR(5) NULL,
		unit_number VARCHAR(3) NULL,
		street TEXT NULL,
		city VARCHAR(40) NULL,
		state VARCHAR(30) NULL,
		region VARCHAR(30) NULL,
		country VARCHAR(30) NULL,
		post_code VARCHAR(10) NULL,
		latitude FLOAT NULL,
		longitude FLOAT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_locations_deleted_at (deleted_at)
        );

CREATE TABLE IF NOT EXISTS currencies (
		id INT NOT NULL ,
		name VARCHAR(30) NOT NULL,
		sign VARCHAR(3) NULL,
		portable VARCHAR(6) NULL,
		space BOOL DEFAULT FALSE,
		prepend BOOL DEFAULT TRUE,
		precesion INT NOT NULL DEFAULT 2,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_currencies_deleted_at (deleted_at)
	);

CREATE TABLE IF NOT EXISTS rates (
		id INT NOT NULL ,
		name VARCHAR(10) NOT NULL,
		min INT NOT NULL,
		max INT NOT NULL,
		amount FLOAT NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_plans_deleted_at (deleted_at)
	);

CREATE TABLE IF NOT EXISTS media (
		id INT NOT NULL ,
		type VARCHAR(15) NOT NULL,
		name VARCHAR(30) NOT NULL,
		url TEXT NULL,
		extension VARCHAR(5) NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_media_deleted_at (deleted_at)
	);

CREATE TABLE IF NOT EXISTS contacts (
		id INT NOT NULL ,
		first_name VARCHAR(20) NOT NULL,
		middle_name VARCHAR(20) NULL,
		last_name VARCHAR(20) NOT NULL,
		website VARCHAR(30) NULL,
		note TEXT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_contacts_deleted_at (deleted_at)
	);

CREATE TABLE IF NOT EXISTS emails (
		id INT NOT NULL ,
		email VARCHAR(50) NOT NULL,
		contact_id INT NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_emails_deleted_at (deleted_at),
		INDEX idx_emails_contact_id (contact_id),
		CONSTRAINT fk_contacts_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id)
	);

CREATE TABLE IF NOT EXISTS faxes (
		id INT NOT NULL ,
		number VARCHAR(20) NOT NULL,
		contact_id INT,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_faxes_deleted_at (deleted_at),
		INDEX idx_faxes_contact_id (contact_id),
		CONSTRAINT fk_faxes_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id)
	);

CREATE TABLE IF NOT EXISTS phones (
		id INT NOT NULL ,
		type VARCHAR(10) NULL,
		number VARCHAR(2) NULL,
		contact_id INT,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_phones_deleted_at (deleted_at),
		INDEX idx_phones_contact_id (contact_id),
		CONSTRAINT fk_phones_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id)
	);

CREATE TABLE IF NOT EXISTS media_contacts (
		media_id INT NOT NULL,
		contact_id INT NOT NULL,
		CONSTRAINT `primary` PRIMARY KEY (media_id, contact_id)
	);

CREATE TABLE IF NOT EXISTS people (
		id INT NOT NULL ,
		contact_id INT NULL,
		location_id INT NULL,
		active_since TIMESTAMP NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_people_deleted_at (deleted_at),
		INDEX idx_people_contact_id (contact_id),
		INDEX idx_people_location_id (location_id),
		CONSTRAINT fk_people_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id),
		CONSTRAINT fk_people_location_id FOREIGN KEY (location_id) REFERENCES locations (id)
	);

CREATE TABLE IF NOT EXISTS users (
		id INT NOT NULL ,
		username VARCHAR(50) NULL,
		password TEXT NULL,
		person_id INT NULL,
		roles TEXT,
		permissions TEXT,
		seed VARCHAR(16) NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_users_deleted_at (deleted_at),
		INDEX idx_people_user_id (person_id),
		CONSTRAINT fk_user_person_id FOREIGN KEY (person_id) REFERENCES people (id)
	);

CREATE TABLE IF NOT EXISTS subscriptions (
		id INT NOT NULL ,
		payment_method VARCHAR(10) NULL,
		billing_cycle VARCHAR(10) NULL,
		billing_day VARCHAR(10) NULL,
		status VARCHAR(10) NULL,
		discount FLOAT NULL,
		is_discount_percentage BOOL DEFAULT TRUE,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_subscriptions_deleted_at (deleted_at)
	);

CREATE TABLE IF NOT EXISTS companies (
		id INT NOT NULL ,
		name TEXT NOT NULL,
		company_code TEXT NULL,
		abn TEXT NULL,
		description TEXT NULL,
		verified_on TIMESTAMP NULL,
		subscription_id INT NULL,
		location_id INT NULL,
		owner_id INT NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_companies_deleted_at (deleted_at),
		INDEX idx_companies_subscription_id (subscription_id),
		CONSTRAINT fk_companies_subscription_id FOREIGN KEY (subscription_id) REFERENCES subscriptions (id),
		INDEX idx_companies_location_id (location_id),
		CONSTRAINT fk_companies_location_id FOREIGN KEY (location_id) REFERENCES locations (id),
		INDEX idx_companies_owner_id (owner_id),
		CONSTRAINT fk_companies_owner_id FOREIGN KEY (owner_id) REFERENCES people (id)
        );

CREATE TABLE IF NOT EXISTS sites (
		id INT NOT NULL ,
		name VARCHAR(30) NOT NULL,
		code VARCHAR(8) NULL,
		description TEXT NULL,
		timezone VARCHAR(30) NULL,
		company_id INT NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_sites_deleted_at (deleted_at),
		INDEX idx_sites_company_id (company_id),
		CONSTRAINT fk_sites_company_id FOREIGN KEY (company_id) REFERENCES companies (id)
	);

CREATE TABLE IF NOT EXISTS products (
		id INT NOT NULL ,
		site_id INT NOT NULL,
		name VARCHAR(100) NOT NULL,
		code VARCHAR(40) NOT NULL,
		description TEXT NULL,
		barcode VARCHAR(20) NULL,
		cost_ex_tax FLOAT NULL,
		cost_tax FLOAT NULL,
		is_for_sale BOOL DEFAULT TRUE,
		is_modifier BOOL DEFAULT FALSE,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_products_deleted_at (deleted_at),
		INDEX idx_products_site_id (site_id),
		CONSTRAINT fk_products_site_id FOREIGN KEY (site_id) REFERENCES sites (id)
	);

CREATE TABLE IF NOT EXISTS product_variants (
		id INT NOT NULL ,
		product_id INT NOT NULL,
		variant_id INT NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_product_variants_deleted_at (deleted_at),
		INDEX idx_product_variants_product_id (product_id),
		INDEX idx_product_variants_variant_id (variant_id),
		CONSTRAINT fk_product_variants_product_id FOREIGN KEY (product_id) REFERENCES products (id),
		CONSTRAINT fk_product_variants_variant_id FOREIGN KEY (variant_id) REFERENCES products (id)
	);

CREATE TABLE IF NOT EXISTS product_modifiers (
		product_id INT NOT NULL,
		modifier_id INT NOT NULL,
		cost_ex_tax FLOAT NULL,
		cost_tax FLOAT NULL,
		mandatory BOOL DEFAULT FALSE,
		alternatives TEXT,
		CONSTRAINT `primary` PRIMARY KEY (product_id, modifier_id),
		INDEX idx_product_modifiers_product_id (product_id),
		INDEX idx_product_modifiers_modifier_id (modifier_id),
		CONSTRAINT fk_product_modifiers_product_id FOREIGN KEY (product_id) REFERENCES products (id),
		CONSTRAINT fk_product_modifiers_variant_id FOREIGN KEY (modifier_id) REFERENCES products (id)
	);

CREATE TABLE IF NOT EXISTS tables (
		id INT NOT NULL ,
		site_id INT NOT NULL,
		name VARCHAR(30) NOT NULL,
		code VARCHAR(10) NULL,
		barcode TEXT NOT NULL,
		section_name VARCHAR(20) NULL,
		sort_order INT NULL,
		available BOOL DEFAULT TRUE,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_tables_deleted_at (deleted_at),
		INDEX idx_tables_site_id (site_id),
		CONSTRAINT fk_tables_site_id FOREIGN KEY (site_id) REFERENCES sites (id)
	);

CREATE TABLE IF NOT EXISTS orders (
		id INT NOT NULL ,
		site_id INT NOT NULL,
		voided BOOL DEFAULT FALSE,
		notes TEXT NULL,
		status VARCHAR(10) NULL,
		placed_at TIMESTAMP NULL,
		completed_at TIMESTAMP NULL,
		total_ex_tax FLOAT DEFAULT 0,
		total_tax FLOAT DEFAULT 0,
		discount FLOAT DEFAULT 0,
		is_discount_percentage BOOL DEFAULT TRUE,
		chat_history TEXT NULL,
		estimated_time INT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_orders_deleted_at (deleted_at),
		INDEX idx_orders_site_id (site_id),
		CONSTRAINT fk_orders_site_id FOREIGN KEY (site_id) REFERENCES sites (id)
	);

CREATE TABLE IF NOT EXISTS invoices (
		id INT NOT NULL ,
		company_id INT NOT NULL,
		site_id INT NOT NULL,
		code VARCHAR(10) NULL,
		date TIMESTAMP NOT NULL,
		start_date TIMESTAMP NULL,
		amount_ex_tax FLOAT NULL,
		amount_tax FLOAT NULL,
		credit_amount FLOAT NULL,
		status VARCHAR(10) NULL,
		description TEXT NULL,
		processed_at TIMESTAMP NULL,
		content TEXT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP NULL,
		CONSTRAINT `primary` PRIMARY KEY (id),
		INDEX idx_invoices_deleted_at (deleted_at),
		INDEX idx_invoices_site_id (site_id),
		CONSTRAINT fk_invoices_site_id FOREIGN KEY (site_id) REFERENCES sites (id),
		INDEX idx_invoices_company_id (company_id),
		CONSTRAINT fk_invoices_company_id FOREIGN KEY (company_id) REFERENCES companies (id)
	);

