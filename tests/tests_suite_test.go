package tests

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
	"api/libs/database"
	"os"
	"api/libs/system"

	_ "github.com/lib/pq"
	"api/migration/bootstrap"
	"api/tests/setup"
)

func TestTests(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Tests Suite")
}

var _ = BeforeSuite(func() {
	os.Setenv(system.Env, system.EnvTest)
	database.DB, _ = database.GetDB()

	// Refresh Database
	bootstrap.Drop(database.DB)
	bootstrap.Create(database.DB)

	// Bootstrap Data
	setup.PopulateData("../../api/datas")
})

var _ = AfterSuite(func() {
	database.DB.Close()
})