package setup

import (
	"api/commands/command"
	"api/libs/database"
	"api/models"
	"io/ioutil"
	"fmt"
	"os"
	"path/filepath"
)

// PopulateData ...
func PopulateData(pathToDatas string) {
	command.CreateClient(database.DB)
	command.CreateUser(database.DB)
	var owner models.User
	database.DB.Where("username = ?", "ir.owner@yopmail.com").First(&owner)
	person := owner.GetPerson()
	company := command.CreateCompany(database.DB, person)
	site := command.CreateSite(database.DB, company, person)

	// Create products
	CreateProduct(site, pathToDatas)
}

// CreateProduct ...
func CreateProduct(site models.Site, pathToDatas string ) {
	file, err := filepath.Abs(pathToDatas + "/tests/products.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	productJSON, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	command.CreateProducts(database.DB, site, string(productJSON))
}